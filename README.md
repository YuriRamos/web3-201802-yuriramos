# Web III: Trabalho Final
Sistema de RPG de Browser:
* Criação de Personagens
* Itens
* Monstros

### Prerequisites
Ruby on Rails
* Comandos para instalação no Linux:

 * sudo apt-get install ruby
 * gem install rails

* Windows basta executar os instaladores encontrados nos seguintes endereços:

 * https://rubyinstaller.org/
 * http://www.railsinstaller.org/pt-BR

## Built With

* [Rails](https://rubyonrails.org/) - Rails


## Authors
Yuri Ramos Canário Campos