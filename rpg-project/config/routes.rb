Rails.application.routes.draw do
  resources :skills
  resources :monsters
  resources :characters
  resources :weapons
  resources :vocations
  resources :secondaries
  resources :races
  post 'character/buyconfirm'
  get 'character/buy/:id', to: 'characters#buy'
  resources :helmets
  resources :armors
  devise_for :users, path: '', path_names: {
                                            sign_in: 'login',
                                            sign_up: 'register',
                                            sign_out: 'logout'
                                          }
  resources :companies
  resources :kinds

  # get 'home/index'
  # get 'home/chuchu', to: 'home#index'
  root 'home#index'

  resources :addresses
  resources :phones
  resources :kinds, except: [:destroy]
  # resources(:kinds, {:except => [:destroy, :update, :edit]})

  resources :pets, only: [:index, :show, :new, :edit]
  # resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
