require 'test_helper'

class HelmetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @helmet = helmets(:one)
  end

  test "should get index" do
    get helmets_url
    assert_response :success
  end

  test "should get new" do
    get new_helmet_url
    assert_response :success
  end

  test "should create helmet" do
    assert_difference('Helmet.count') do
      post helmets_url, params: { helmet: { armor: @helmet.armor, cons_requirement: @helmet.cons_requirement, int_bonus: @helmet.int_bonus, int_requirement: @helmet.int_requirement, name: @helmet.name, str_requirement: @helmet.str_requirement, value: @helmet.value } }
    end

    assert_redirected_to helmet_url(Helmet.last)
  end

  test "should show helmet" do
    get helmet_url(@helmet)
    assert_response :success
  end

  test "should get edit" do
    get edit_helmet_url(@helmet)
    assert_response :success
  end

  test "should update helmet" do
    patch helmet_url(@helmet), params: { helmet: { armor: @helmet.armor, cons_requirement: @helmet.cons_requirement, int_bonus: @helmet.int_bonus, int_requirement: @helmet.int_requirement, name: @helmet.name, str_requirement: @helmet.str_requirement, value: @helmet.value } }
    assert_redirected_to helmet_url(@helmet)
  end

  test "should destroy helmet" do
    assert_difference('Helmet.count', -1) do
      delete helmet_url(@helmet)
    end

    assert_redirected_to helmets_url
  end
end
