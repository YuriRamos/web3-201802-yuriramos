require 'test_helper'

class SecondariesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @secondary = secondaries(:one)
  end

  test "should get index" do
    get secondaries_url
    assert_response :success
  end

  test "should get new" do
    get new_secondary_url
    assert_response :success
  end

  test "should create secondary" do
    assert_difference('Secondary.count') do
      post secondaries_url, params: { secondary: { armor: @secondary.armor, damage: @secondary.damage, dex_requirement: @secondary.dex_requirement, int_requirement: @secondary.int_requirement, name: @secondary.name, str_requirement: @secondary.str_requirement, value: @secondary.value } }
    end

    assert_redirected_to secondary_url(Secondary.last)
  end

  test "should show secondary" do
    get secondary_url(@secondary)
    assert_response :success
  end

  test "should get edit" do
    get edit_secondary_url(@secondary)
    assert_response :success
  end

  test "should update secondary" do
    patch secondary_url(@secondary), params: { secondary: { armor: @secondary.armor, damage: @secondary.damage, dex_requirement: @secondary.dex_requirement, int_requirement: @secondary.int_requirement, name: @secondary.name, str_requirement: @secondary.str_requirement, value: @secondary.value } }
    assert_redirected_to secondary_url(@secondary)
  end

  test "should destroy secondary" do
    assert_difference('Secondary.count', -1) do
      delete secondary_url(@secondary)
    end

    assert_redirected_to secondaries_url
  end
end
