require "application_system_test_case"

class HelmetsTest < ApplicationSystemTestCase
  setup do
    @helmet = helmets(:one)
  end

  test "visiting the index" do
    visit helmets_url
    assert_selector "h1", text: "Helmets"
  end

  test "creating a Helmet" do
    visit helmets_url
    click_on "New Helmet"

    fill_in "Armor", with: @helmet.armor
    fill_in "Cons Requirement", with: @helmet.cons_requirement
    fill_in "Int Bonus", with: @helmet.int_bonus
    fill_in "Int Requirement", with: @helmet.int_requirement
    fill_in "Name", with: @helmet.name
    fill_in "Str Requirement", with: @helmet.str_requirement
    fill_in "Value", with: @helmet.value
    click_on "Create Helmet"

    assert_text "Helmet was successfully created"
    click_on "Back"
  end

  test "updating a Helmet" do
    visit helmets_url
    click_on "Edit", match: :first

    fill_in "Armor", with: @helmet.armor
    fill_in "Cons Requirement", with: @helmet.cons_requirement
    fill_in "Int Bonus", with: @helmet.int_bonus
    fill_in "Int Requirement", with: @helmet.int_requirement
    fill_in "Name", with: @helmet.name
    fill_in "Str Requirement", with: @helmet.str_requirement
    fill_in "Value", with: @helmet.value
    click_on "Update Helmet"

    assert_text "Helmet was successfully updated"
    click_on "Back"
  end

  test "destroying a Helmet" do
    visit helmets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Helmet was successfully destroyed"
  end
end
