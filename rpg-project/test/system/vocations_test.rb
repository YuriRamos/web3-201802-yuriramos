require "application_system_test_case"

class VocationsTest < ApplicationSystemTestCase
  setup do
    @vocation = vocations(:one)
  end

  test "visiting the index" do
    visit vocations_url
    assert_selector "h1", text: "Vocations"
  end

  test "creating a Vocation" do
    visit vocations_url
    click_on "New Vocation"

    fill_in "Minimum Level", with: @vocation.minimum_level
    fill_in "Name", with: @vocation.name
    fill_in "Playable", with: @vocation.playable
    click_on "Create Vocation"

    assert_text "Vocation was successfully created"
    click_on "Back"
  end

  test "updating a Vocation" do
    visit vocations_url
    click_on "Edit", match: :first

    fill_in "Minimum Level", with: @vocation.minimum_level
    fill_in "Name", with: @vocation.name
    fill_in "Playable", with: @vocation.playable
    click_on "Update Vocation"

    assert_text "Vocation was successfully updated"
    click_on "Back"
  end

  test "destroying a Vocation" do
    visit vocations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Vocation was successfully destroyed"
  end
end
