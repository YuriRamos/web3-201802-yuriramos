require "application_system_test_case"

class SecondariesTest < ApplicationSystemTestCase
  setup do
    @secondary = secondaries(:one)
  end

  test "visiting the index" do
    visit secondaries_url
    assert_selector "h1", text: "Secondaries"
  end

  test "creating a Secondary" do
    visit secondaries_url
    click_on "New Secondary"

    fill_in "Armor", with: @secondary.armor
    fill_in "Damage", with: @secondary.damage
    fill_in "Dex Requirement", with: @secondary.dex_requirement
    fill_in "Int Requirement", with: @secondary.int_requirement
    fill_in "Name", with: @secondary.name
    fill_in "Str Requirement", with: @secondary.str_requirement
    fill_in "Value", with: @secondary.value
    click_on "Create Secondary"

    assert_text "Secondary was successfully created"
    click_on "Back"
  end

  test "updating a Secondary" do
    visit secondaries_url
    click_on "Edit", match: :first

    fill_in "Armor", with: @secondary.armor
    fill_in "Damage", with: @secondary.damage
    fill_in "Dex Requirement", with: @secondary.dex_requirement
    fill_in "Int Requirement", with: @secondary.int_requirement
    fill_in "Name", with: @secondary.name
    fill_in "Str Requirement", with: @secondary.str_requirement
    fill_in "Value", with: @secondary.value
    click_on "Update Secondary"

    assert_text "Secondary was successfully updated"
    click_on "Back"
  end

  test "destroying a Secondary" do
    visit secondaries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Secondary was successfully destroyed"
  end
end
