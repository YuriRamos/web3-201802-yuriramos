require "application_system_test_case"

class MonstersTest < ApplicationSystemTestCase
  setup do
    @monster = monsters(:one)
  end

  test "visiting the index" do
    visit monsters_url
    assert_selector "h1", text: "Monsters"
  end

  test "creating a Monster" do
    visit monsters_url
    click_on "New Monster"

    fill_in "Armor", with: @monster.armor
    fill_in "Constitution", with: @monster.constitution
    fill_in "Dexterity", with: @monster.dexterity
    fill_in "Exp", with: @monster.exp
    fill_in "Health", with: @monster.health
    fill_in "Inteligence", with: @monster.inteligence
    fill_in "Money", with: @monster.money
    fill_in "Name", with: @monster.name
    fill_in "Race", with: @monster.race_id
    fill_in "Strength", with: @monster.strength
    fill_in "Vocation", with: @monster.vocation_id
    click_on "Create Monster"

    assert_text "Monster was successfully created"
    click_on "Back"
  end

  test "updating a Monster" do
    visit monsters_url
    click_on "Edit", match: :first

    fill_in "Armor", with: @monster.armor
    fill_in "Constitution", with: @monster.constitution
    fill_in "Dexterity", with: @monster.dexterity
    fill_in "Exp", with: @monster.exp
    fill_in "Health", with: @monster.health
    fill_in "Inteligence", with: @monster.inteligence
    fill_in "Money", with: @monster.money
    fill_in "Name", with: @monster.name
    fill_in "Race", with: @monster.race_id
    fill_in "Strength", with: @monster.strength
    fill_in "Vocation", with: @monster.vocation_id
    click_on "Update Monster"

    assert_text "Monster was successfully updated"
    click_on "Back"
  end

  test "destroying a Monster" do
    visit monsters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Monster was successfully destroyed"
  end
end
