# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_28_214210) do

  create_table "armors", force: :cascade do |t|
    t.integer "armor"
    t.integer "cons_requirement"
    t.string "name"
    t.integer "str_requirement"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "characters", force: :cascade do |t|
    t.integer "age"
    t.integer "exp", default: 0
    t.string "genre"
    t.integer "level", default: 1
    t.integer "money", default: 2000
    t.string "name"
    t.integer "health"
    t.integer "user_id"
    t.integer "armor_id"
    t.integer "helmet_id"
    t.integer "race_id"
    t.integer "secondary_id"
    t.integer "vocation_id"
    t.integer "weapon_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["armor_id"], name: "index_characters_on_armor_id"
    t.index ["helmet_id"], name: "index_characters_on_helmet_id"
    t.index ["race_id"], name: "index_characters_on_race_id"
    t.index ["secondary_id"], name: "index_characters_on_secondary_id"
    t.index ["user_id"], name: "index_characters_on_user_id"
    t.index ["vocation_id"], name: "index_characters_on_vocation_id"
    t.index ["weapon_id"], name: "index_characters_on_weapon_id"
  end

  create_table "helmets", force: :cascade do |t|
    t.integer "armor"
    t.integer "cons_requirement"
    t.integer "int_bonus"
    t.integer "int_requirement"
    t.string "name"
    t.integer "str_requirement"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "monsters", force: :cascade do |t|
    t.string "name"
    t.integer "health"
    t.integer "exp"
    t.integer "armor"
    t.integer "money"
    t.integer "strength"
    t.integer "inteligence"
    t.integer "dexterity"
    t.integer "constitution"
    t.integer "vocation_id"
    t.integer "race_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["race_id"], name: "index_monsters_on_race_id"
    t.index ["vocation_id"], name: "index_monsters_on_vocation_id"
  end

  create_table "races", force: :cascade do |t|
    t.integer "constitution"
    t.integer "dexterity"
    t.integer "inteligence"
    t.string "name"
    t.boolean "playable"
    t.integer "strength"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "secondaries", force: :cascade do |t|
    t.integer "armor"
    t.integer "damage"
    t.integer "dex_requirement"
    t.integer "int_requirement"
    t.string "name"
    t.integer "str_requirement"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skills", force: :cascade do |t|
    t.string "name"
    t.integer "damage"
    t.integer "heal"
    t.integer "str_modifier"
    t.integer "dex_modifier"
    t.integer "int_modifier"
    t.integer "cons_modifier"
    t.integer "level"
    t.integer "vocation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["vocation_id"], name: "index_skills_on_vocation_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name"
    t.integer "kind"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vocations", force: :cascade do |t|
    t.integer "minimum_level"
    t.string "name"
    t.boolean "playable"
    t.integer "strength_bonus", default: 1
    t.integer "inteligence_bonus", default: 1
    t.integer "dexterity_bonus", default: 1
    t.integer "constitution_bonus", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "weapons", force: :cascade do |t|
    t.integer "damage"
    t.integer "dex_requirement"
    t.integer "int_requirement"
    t.string "name"
    t.integer "str_requirement"
    t.boolean "two_handed"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
