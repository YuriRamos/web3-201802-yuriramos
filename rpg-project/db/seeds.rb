require 'faker'

5.times do
	Vocation.create! minimum_level: 0, name: Faker::Superhero.unique.prefix,playable: true
end

10.times do
	Race.create! name: Faker::StarWars.unique.specie, constitution: 1+rand(10) ,dexterity: 1+rand(10), inteligence: 1+rand(10),strength: 1+rand(10),playable: true
end

Armor.create! name: 'Plate Armor', cons_requirement: 8, str_requirement: 0,value: 200, armor: 10
Armor.create! name: 'Dragon Robe', cons_requirement: 10, str_requirement: 0,value: 500, armor: 12

Weapon.create! name: 'Regular Sword', damage: 8, dex_requirement: 0, int_requirement: 0, str_requirement: 5, two_handed: false, value: 100

Weapon.create! name: 'Wood Staff', damage: 10, dex_requirement: 0, int_requirement: 9, str_requirement: 5, two_handed: true, value: 200 