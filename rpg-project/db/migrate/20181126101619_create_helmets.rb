class CreateHelmets < ActiveRecord::Migration[5.2]
  def change
    create_table :helmets do |t|
      t.integer :armor
      t.integer :cons_requirement
      t.integer :int_bonus
      t.integer :int_requirement
      t.string :name
      t.integer :str_requirement
      t.integer :value

      t.timestamps
    end
  end
end
