class CreateSecondaries < ActiveRecord::Migration[5.2]
  def change
    create_table :secondaries do |t|
      t.integer :armor
      t.integer :damage
      t.integer :dex_requirement
      t.integer :int_requirement
      t.string :name
      t.integer :str_requirement
      t.integer :value

      t.timestamps
    end
  end
end
