class CreateCharacters < ActiveRecord::Migration[5.2]
  def change
    create_table :characters do |t|
      t.integer :age
      t.integer :exp, default: 0
      t.string :genre
      t.integer :level, default: 1
      t.integer :money, default: 2000
      t.string :name
      t.integer :health
      t.references :user, foreign_key: true
      t.references :armor, foreign_key: true
      t.references :helmet, foreign_key: true
      t.references :race, foreign_key: true
      t.references :secondary, foreign_key: true
      t.references :vocation, foreign_key: true
      t.references :weapon, foreign_key: true

      t.timestamps
    end
  end
end
