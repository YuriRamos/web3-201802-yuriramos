class CreateRaces < ActiveRecord::Migration[5.2]
  def change
    create_table :races do |t|
      t.integer :constitution
      t.integer :dexterity
      t.integer :inteligence
      t.string :name
      t.boolean :playable
      t.integer :strength

      t.timestamps
    end
  end
end
