class CreateWeapons < ActiveRecord::Migration[5.2]
  def change
    create_table :weapons do |t|
      t.integer :damage
      t.integer :dex_requirement
      t.integer :int_requirement
      t.string :name
      t.integer :str_requirement
      t.boolean :two_handed
      t.integer :value

      t.timestamps
    end
  end
end
