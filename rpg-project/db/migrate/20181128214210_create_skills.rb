class CreateSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :skills do |t|
      t.string :name
      t.integer :damage
      t.integer :heal
      t.integer :str_modifier
      t.integer :dex_modifier
      t.integer :int_modifier
      t.integer :cons_modifier
      t.integer :level
      t.references :vocation, foreign_key: true

      t.timestamps
    end
  end
end
