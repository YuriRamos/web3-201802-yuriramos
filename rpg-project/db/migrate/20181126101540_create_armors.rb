class CreateArmors < ActiveRecord::Migration[5.2]
  def change
    create_table :armors do |t|
      t.integer :armor
      t.integer :cons_requirement
      t.string :name
      t.integer :str_requirement
      t.integer :value

      t.timestamps
    end
  end
end
