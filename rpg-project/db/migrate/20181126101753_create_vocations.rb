class CreateVocations < ActiveRecord::Migration[5.2]
  def change
    create_table :vocations do |t|
      t.integer :minimum_level
      t.string :name
      t.boolean :playable

      t.integer :strength_bonus, default: 1
      t.integer :inteligence_bonus, default: 1
      t.integer :dexterity_bonus, default: 1
      t.integer :constitution_bonus, default: 1

      t.timestamps
    end
  end
end
