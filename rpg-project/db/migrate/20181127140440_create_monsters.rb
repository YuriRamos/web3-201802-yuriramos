class CreateMonsters < ActiveRecord::Migration[5.2]
  def change
    create_table :monsters do |t|
      t.string :name
      t.integer :health
      t.integer :exp
      t.integer :armor
      t.integer :money
      t.integer :strength
      t.integer :inteligence
      t.integer :dexterity
      t.integer :constitution
      t.references :vocation, foreign_key: true
      t.references :race, foreign_key: true

      t.timestamps
    end
  end
end
