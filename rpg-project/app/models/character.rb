class Character < ApplicationRecord
	belongs_to :user
	belongs_to :weapon, required: false
	belongs_to :armor, required: false
	belongs_to :race
	belongs_to :secondary, required: false
	belongs_to :helmet, required: false
	belongs_to :race
	belongs_to :vocation

end
