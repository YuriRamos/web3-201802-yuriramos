class Vocation < ApplicationRecord
	has_many :characters
	has_many :skills
end
