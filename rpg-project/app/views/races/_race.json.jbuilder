json.extract! race, :id, :constitution, :dexterity, :inteligence, :name, :playable, :strength, :created_at, :updated_at
json.url race_url(race, format: :json)
