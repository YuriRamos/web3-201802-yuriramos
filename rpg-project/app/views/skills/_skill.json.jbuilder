json.extract! skill, :id, :name, :damage, :heal, :str_modifier, :dex_modifier, :int_modifier, :cons_modifier, :level, :vocation_id, :created_at, :updated_at
json.url skill_url(skill, format: :json)
