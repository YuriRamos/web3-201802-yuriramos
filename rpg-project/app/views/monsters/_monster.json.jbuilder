json.extract! monster, :id, :name, :health, :exp, :armor, :money, :strength, :inteligence, :dexterity, :constitution, :vocation_id, :race_id, :created_at, :updated_at
json.url monster_url(monster, format: :json)
