json.extract! secondary, :id, :armor, :damage, :dex_requirement, :int_requirement, :name, :str_requirement, :value, :created_at, :updated_at
json.url secondary_url(secondary, format: :json)
