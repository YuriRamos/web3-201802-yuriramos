json.extract! helmet, :id, :armor, :cons_requirement, :int_bonus, :int_requirement, :name, :str_requirement, :value, :created_at, :updated_at
json.url helmet_url(helmet, format: :json)
