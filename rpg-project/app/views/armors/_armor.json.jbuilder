json.extract! armor, :id, :armor, :cons_requirement, :name, :str_requirement, :value, :created_at, :updated_at
json.url armor_url(armor, format: :json)
