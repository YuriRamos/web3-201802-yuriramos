json.extract! vocation, :id, :minimum_level, :name, :playable, :created_at, :updated_at
json.url vocation_url(vocation, format: :json)
