json.extract! character, :id, :age, :exp, :genre, :level, :money, :name,:user_id, :race_id, :vocation_id, :created_at, :updated_at
json.url character_url(character, format: :json)
