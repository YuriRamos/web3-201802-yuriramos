json.extract! weapon, :id, :damage, :dex_requirement, :int_requirement, :name, :str_requirement, :two_handed, :value, :created_at, :updated_at
json.url weapon_url(weapon, format: :json)
