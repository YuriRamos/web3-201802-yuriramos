class VocationsController < ApplicationController
  before_action :set_vocation, only: [:show, :edit, :update, :destroy]

  # GET /vocations
  # GET /vocations.json
  def index
    @vocations = Vocation.all
  end

  # GET /vocations/1
  # GET /vocations/1.json
  def show
  end

  # GET /vocations/new
  def new
    if current_user.kind == 1
    @vocation = Vocation.new
    else
      not_authorized
    end
  end

  # GET /vocations/1/edit
  def edit
    if current_user.kind == 0
      not_authorized
    end
  end

  # POST /vocations
  # POST /vocations.json
  def create
    @vocation = Vocation.new(vocation_params)

    respond_to do |format|
      if @vocation.save
        format.html { redirect_to @vocation, notice: 'Vocation was successfully created.' }
        format.json { render :show, status: :created, location: @vocation }
      else
        format.html { render :new }
        format.json { render json: @vocation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vocations/1
  # PATCH/PUT /vocations/1.json
  def update
    respond_to do |format|
      if @vocation.update(vocation_params)
        format.html { redirect_to @vocation, notice: 'Vocation was successfully updated.' }
        format.json { render :show, status: :ok, location: @vocation }
      else
        format.html { render :edit }
        format.json { render json: @vocation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vocations/1
  # DELETE /vocations/1.json
  def destroy
    if current_user.kind == 1
      @vocation.destroy
      respond_to do |format|
        format.html { redirect_to vocations_url, notice: 'Vocation was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      not_authorized
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vocation
      @vocation = Vocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vocation_params
      params.require(:vocation).permit(:minimum_level, :name, :playable, :strength_bonus, :inteligence_bonus, :dexterity_bonus, :constitution_bonus)
    end
end
