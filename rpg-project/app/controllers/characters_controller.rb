class CharactersController < ApplicationController
  before_action :set_character, only: [:show, :edit, :update, :destroy, :buy, :strengthtotal, :dexteritytotal, :inteligencetotal, :constitutiontotal]

  # GET /characters
  # GET /characters.json
  def index
    if current_user.kind == 1
    @characters = Character.all
    else
      @characters = Character.where(:user_id => current_user.id)
    end
  end

  # GET /characters/1
  # GET /characters/1.json
  def show
  end

  # GET /characters/new
  def new
    @character = Character.new
  end

  def buy
    @character = Character.find(params[:id])
  end

  # GET /characters/1/edit
  def edit
    if current_user.id != @character.user.id and current_user.kind == 0
      not_authorized
    end
  end

  # POST /characters
  # POST /characters.json
  def create
    @character = Character.new(character_params.merge(user_id: current_user.id))
    @raca_id = @character.race_id
    @raca = Race.find(@raca_id)
    @character.health = @raca.constitution*10
    respond_to do |format|
      if @character.save
        format.html { redirect_to @character, notice: 'Character was successfully created.' }
        format.json { render :show, status: :created, location: @character }
      else
        format.html { render :new }
        format.json { render json: @character.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /characters/1
  # PATCH/PUT /characters/1.json
  def update
    @certo = true
    @helmetnovo = character_params[:helmet_id].to_i
    @armornovo = character_params[:armor_id].to_i
    @weaponnovo = character_params[:weapon_id].to_i
    @secondarynovo = character_params[:secondary_id].to_i

    if @helmetnovo.nil? or @helmetnovo == 0
    else
      if @character.helmet.nil?
        @helmet = Helmet.find(@helmetnovo)
        @character.money = @character.money - @helmet.value
        if @character.money < @helmet.value
          @certo = false
        end
      elsif @character.helmet.id.to_i != @helmetnovo
        @helmet = Helmet.find(@helmetnovo)
        @character.money = @character.money - @helmet.value
        if @character.money < @helmet.value
          @certo = false
        end     
      end
    end

    if @armornovo.nil? or @armornovo == 0
    else
      if @character.armor.nil?
        @armor = Armor.find(@armornovo)
        @character.money = @character.money - @armor.value
        if @character.money < @armor.value
          @certo = false
        end
      elsif @character.armor.id != @armornovo
        @armor = Armor.find(@armornovo)
        @character.money = @character.money - @armor.value
        if @character.money < @armor.value
          @certo = false
        end
      end
    end

    if @weaponnovo.nil? or @weaponnovo == 0
    else
      puts('Arma nova: ')
      puts(@weaponnovo)
      if @character.weapon.nil?
        @weapon = Weapon.find(@weaponnovo)
        @character.money = @character.money - @weapon.value
        if @character.money < @weapon.value
          @certo = false
        end
      elsif @character.weapon.id.to_i != @weaponnovo
        @weapon = Weapon.find(@weaponnovo)
        @character.money = @character.money - @weapon.value
        if @character.money < @weapon.value
          @certo = false
        end
      end
    end

    if @secondarynovo.nil? or @secondarynovo == 0
    else
      puts('Secundaria nova: ')
      puts(@secondarynovo)
      if @character.secondary.nil?
        @secondary = Secondary.find(@secondarynovo)
        @character.money = @character.money - @secondary.value
        if @character.money < @secondary.value
          @certo = false
        end
      elsif @character.secondary.id.to_i != @secondarynovo
        @secondary = Secondary.find(@secondarynovo)
        @character.money = @character.money - @secondary.value
        if @character.money < @secondary.value
          @certo = false
        end
      end
    end
    puts('-------')
    puts(character_params[:helmet_id])
    puts(character_params[:armor_id])
    puts(character_params[:weapon_id])
    puts(character_params[:secondary_id])
    puts('-------')
      respond_to do |format|
        if @certo == true
          if @character.update(character_params)
            format.html { redirect_to @character, notice: 'Character was successfully updated.' }
            format.json { render :show, status: :ok, location: @character }
          else
            format.html { render :edit }
            format.json { render json: @character.errors, status: :unprocessable_entity }
          end
        else
          format.html {redirect_to @character, notice: 'You dont have that money !'}
        end
      end
  end

  # DELETE /characters/1
  # DELETE /characters/1.json
  def destroy
    @character.destroy
    respond_to do |format|
      format.html { redirect_to characters_url, notice: 'Character was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_character
      @character = Character.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def character_params
      params.require(:character).permit(:age, :exp, :genre, :level, :money, :name, :race_id, :user_id, :vocation_id, :health, :helmet_id, :armor_id, :weapon_id, :secondary_id)
    end
end
