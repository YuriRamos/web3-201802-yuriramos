class HelmetsController < ApplicationController
  before_action :set_helmet, only: [:show, :edit, :update, :destroy]

  # GET /helmets
  # GET /helmets.json
  def index
    @helmets = Helmet.all
  end

  # GET /helmets/1
  # GET /helmets/1.json
  def show
  end
  # GET /helmets/new
  def new
    if current_user.kind == 1
    @helmet = Helmet.new
    else
      not_authorized
    end
  end

  # GET /helmets/1/edit
  def edit
    if current_user.kind == 0
      not_authorized
    end
  end

  # POST /helmets
  # POST /helmets.json
  def create
    @helmet = Helmet.new(helmet_params)

    respond_to do |format|
      if @helmet.save
        format.html { redirect_to @helmet, notice: 'Helmet was successfully created.' }
        format.json { render :show, status: :created, location: @helmet }
      else
        format.html { render :new }
        format.json { render json: @helmet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /helmets/1
  # PATCH/PUT /helmets/1.json
  def update
    respond_to do |format|
      if @helmet.update(helmet_params)
        format.html { redirect_to @helmet, notice: 'Helmet was successfully updated.' }
        format.json { render :show, status: :ok, location: @helmet }
      else
        format.html { render :edit }
        format.json { render json: @helmet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /helmets/1
  # DELETE /helmets/1.json
  def destroy
    if current_user.kind == 1
      @helmet.destroy
      respond_to do |format|
        format.html { redirect_to helmets_url, notice: 'Helmet was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      not_authorized
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_helmet
      @helmet = Helmet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def helmet_params
      params.require(:helmet).permit(:armor, :cons_requirement, :int_bonus, :int_requirement, :name, :str_requirement, :value, :character_id)
    end
end
